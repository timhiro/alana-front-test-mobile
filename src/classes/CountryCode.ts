export class CountryCode {

    public id: number;
    
    public code: string = '+00';
    
    public location: number;
    
    public numberLength: number;

    constructor(_id?: number, _code?: string, _location?: number, _numberLength?: number) {
        this.id = _id;
        this.code = _code;
        this.location = _location;
        this.numberLength = _numberLength;
    }

}