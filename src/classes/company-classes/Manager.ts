import { User } from "../User";
import { Company } from "./Company";
import { CountryCode } from "../CountryCode";
import { Enterprise } from "./Enterprise";
import { Phone } from "../Phone";

export class Manager extends User {

    private _uid: string;
    get uid(): string { return this._uid; };
    set uid(_uid: string) { this._uid = _uid; };

    private _name: string;
    get name(): string { return this._name; };
    set name(_name: string) { this._name = _name; };

    private _lastName: string;
    get lastName(): string { return this._lastName; };
    set lastName(_lastName: string) { this._lastName = _lastName; };

    public _enterprise: Enterprise;
    get enterprise(): Enterprise { return this._enterprise; };
    set enterprise(_enterprise: Enterprise) { this._enterprise = _enterprise; };

    private _phone: Phone;
    get phone(): Phone { return this._phone; };
    set phone(_phone: Phone) { this._phone = _phone; };

    private _formatFullName: string;
    get formatFullName(): string { return this._formatFullName; };

    constructor() {
        super()
        this._name = '';
        this._lastName = '';
        this._enterprise = new Enterprise();
        this._phone = new Phone();
    }

    public static toServerRequest(_instance: Manager): any {
        let _data = {
            name: _instance.name,
            lastName: _instance.lastName,
            email: _instance.email,
            countryCode: _instance.phone.countryCode.id,
            phone: _instance.phone.phoneNumber,
        }

        if (_instance.password !== '') {
            _data['password'] = _instance.password,
                _data['passwordRepeated'] = _instance.password
        }

        return _data;
    }

    public static toClientObject(_data: any, _authorization?: boolean): Manager {
        let _manager = new Manager();
        _manager.name = _data.name;
        _manager.lastName = _data.lastName;
        _manager.email = _data.email;
        _manager.uid = _data.uid;

        if (_data.phone) {
            _manager.phone = new Phone();
            _manager.phone.countryCode.id = _data.phone.countryCodeId;
            _manager.phone.countryCode.code = _data.phone.countryCode;
            _manager.phone.phoneNumber = _data.phone.phone;
        }

        if (_data.companies) {
            _manager.enterprise = Enterprise.toClientObject(_data);
        }

        _manager.setFormatFullName();
        return _manager;
    }

    /**
     * 
     */
    public setFormatFullName(): void {
        if (this._name != '' && this._lastName != '') {
            let _firstName: string = '';
            let _secondName: string = '';
            const _firstNameArray: string[] = this._name.split(' ');
            const _lastNameArray: string[] = this._lastName.split(' ');
            _firstNameArray.forEach(
                value => {
                    if (_firstName.length <= 2) {
                        _firstName = `${_firstName}${value} `;
                    }
                }
            );

            _lastNameArray.forEach(
                value => {
                    if (_secondName.length <= 2) {
                        _secondName = _secondName == '' ? `${_secondName}${value}` : ` ${_secondName}${value}`;
                    }
                }
            );

            this._formatFullName = `${_firstName}${_secondName}`;
        }
    }
}