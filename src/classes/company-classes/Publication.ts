import { JobPosition } from "../JobPosition";
import { Schedule } from "../Schedule";
import { ImageTest } from "../ImageTest";


export class Publication {

    private _id: string;
    get id(): string { return this._id; }
    set id(_id: string) { this._id = _id; }

    private _job: JobPosition;
    get job(): JobPosition { return this._job; }
    set job(_job: JobPosition) { this._job = _job; }

    private _schedule: Schedule;
    get schedule(): Schedule { return this._schedule; }
    set schedule(_schedule: Schedule) { this._schedule = _schedule; }

    private _test: ImageTest[] = [];
    get test(): ImageTest[] { return this._test; }
    set test(_test: ImageTest[]) { this._test = _test; }

    private _title: string;
    get title(): string { return this._title; }
    set title(_title: string) { this._title = _title; }

    private _experience: number;
    get experience(): number { return this._experience; }
    set experience(_experience: number) { this._experience = _experience; }

    private _salary: {upper: number, lower: number};
    get salary(): {upper: number, lower: number} { return this._salary; }
    set salary(_salary: {upper: number, lower: number}) { this._salary = _salary; }

    private _tip: number;
    get tip(): number { return this._tip; }
    set tip(_tip: number) { this._tip = _tip; }

    private _requestedGender: number;
    get requestedGender(): number { return this._requestedGender; }
    set requestedGender(_requestedGender: number) { this._requestedGender = _requestedGender; }

    private _ageRange: {min: number, max: number}
    get ageRange(): {min: number, max: number} { return this._ageRange; }
    set ageRange(_ageRange: {min: number, max: number}) { this._ageRange = _ageRange; }

    private _benefits: string
    get benefits(): string { return this._benefits; }
    set benefits(_benefits: string) { this._benefits = _benefits; }

    private _mission: string
    get mission(): string { return this._mission; }
    set mission(_mission: string) { this._mission = _mission; }

    private _requirements: {id: number, name: string}[]
    get requirements(): {id: number, name: string}[] { return this._requirements; }
    set requirements(_requirements: {id: number, name: string}[]) { this._requirements = _requirements; }

    private _status: boolean;
    get status(): boolean { return this._status; }
    set status(_status: boolean) { this._status = _status; }

    constructor(_job?: JobPosition) {
        this._id = '';
        this._job = _job;
        this._schedule = new Schedule();
        this._experience = 0;
        this._tip = null;
        this._salary = {upper: 15000, lower: 3000}
        this._ageRange = {min: null, max: null};
        this._requestedGender = 0;
        this._benefits = '';
        this._requirements = [];
        this._mission = '';
        this._status = true;
    }

    public static toServerRequest(_instance: Publication): any {
        let _answers = [];

        for (let _answer of _instance.test) {
            _answers.push({id: _answer.id})
        }

        let _data = {
            uid: _instance.id,
            title: _instance.title,
            mission: _instance.mission,
            jobPosition: _instance.job.id,
            benefits: _instance.benefits,
            match:{ 
                sex: _instance.requestedGender == 0? null : _instance.requestedGender,
                minAge: _instance.ageRange.min,
                maxAge: _instance.ageRange.max,
                
            },
            requirements: _instance.requirements,
            experience: _instance.experience,
            minHour: _instance.schedule.minAvailability,
            maxHour: _instance.schedule.maxAvailability,
            answers: _answers,
            minRemuneration: _instance.salary.lower,
            maxRemuneration: _instance.salary.upper,
            tip: _instance.tip != null? _instance.tip : 0
        }

        return _data;
    }
}