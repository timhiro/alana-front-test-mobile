import { Publication } from "./Publication";
import { Address } from "../Address";
import { Asset } from "../../util/Asset";

export class Office {

    private _id: number;
    get id(): number { return this._id; }
    set id(_id: number) { this._id = _id; }

    private _publications: Publication[] = [];
    get publications(): Publication[] { return this._publications; }
    set publications(_publications: Publication[]) { this._publications = _publications; }

    private _address: Address;
    get address(): Address { return this._address; }
    set address(_address: Address) { this._address = _address; }

    private _alias: string;
    get alias(): string { return this._alias; }
    set alias(_alias: string) { this._alias = _alias; }

    private _picture: string;
    get picture(): string { return this._picture }
    set picture(_picture: string) { this._picture = _picture; }

    private _publicationsCounts: number;
    get publicationsCounts(): number { return this._publicationsCounts }
    set publicationsCounts(_publicationsCounts: number) { this._publicationsCounts = _publicationsCounts; }

    constructor() {
        this.id = 0;
        this._publications = [];
        this.address = new Address();
        this._alias = '';
        this._picture = Asset.IMAGE_COMPANY_ELEMENTS_OFFICE;
        this._publicationsCounts = 0;
    }

    public static toServerRequest(_instance: Office): any {
        let _data = {
            alias: _instance.alias,
            addressName: _instance.address.name,
            location: _instance.address.countryId,
            latitude: _instance.address.latitude,
            longitude: _instance.address.longitude,
        }

        return _data;
    }

    public static toClientObject(_data: any): Office {
        let _office: Office = new Office();
        _office.id = _data.id;
        _office.alias = _data.alias;
        _office.address.name = _data.addressName;
        _office.address.country = _data.locationName;
        _office.address.countryId = _data.location;
        _office.address.latitude = _data.latitude;
        _office.address.longitude = _data.longitude;
        if (_data.publicationsQuantity) {
            _office.publicationsCounts = _data.publicationsQuantity;
        }
        //_office._publicationsCounter = _data.publicationCount;

        return _office;
    }
}