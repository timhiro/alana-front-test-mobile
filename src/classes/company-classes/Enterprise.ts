import { Company } from './Company';

export class Enterprise {

    private _companies: Company[];
    get companies(): Company[] { return this._companies };
    set companies( _companies: Company[] ) { this._companies = _companies };

    private _name: string;
    get name(): string { return this._name }
    set name(_name: string) { this._name = _name }

    private _taxId: string;
    get taxId(): string { return this._taxId }
    set taxId(_taxId: string) { this._taxId = _taxId }

    private _currentCompany: Company;
    get currentCompany(): Company { return this._currentCompany; }

    constructor() {
        this._companies = [];
        this._name = '';
        this._taxId = '';
        this.companies.push(new Company());
    };
    

    public static toServerRequest(_instance: Enterprise): any {
        let _data = {
            name: _instance.name,
            taxId: _instance.taxId,
            company: {
                name: _instance.getCompany(0).companyName
            }
        }

        return _data;
    }

    public static toClientObject(_data: any): Enterprise {
        let _enterprise: Enterprise = new Enterprise();

        if (_data.companies) {
            _enterprise.companies = [];
            for(let _companyData of _data.companies) {
                _enterprise.companies.push(Company.toClientObject(_companyData));
            }
        }

        if (_data.enterprise) {
            _enterprise.name = _data.enterprise.name;
            _enterprise.taxId = _data.enterprise.taxId;

        }

        return _enterprise;
    }
    
    /**
     * 
     */
    public getCompany(_index?: number, _id?: string): Company {
        if (_index != null && (_index >= 0 && _index < this.companies.length )) {
            return this.companies[_index ];
        } else if (_id) {
            return this.companies.find(c => c.id == _id);
        }
    }

    /**
     * 
     */
    public getAllCompanyOfficesCount(): number {
        let _count: number = 0;
        for(let _company of this._companies) {
            _count += _company.offices.length;
        }

        return _count;
    }
}
