import { Office } from "./Office";
import { Asset } from "../../util/Asset";

export class Company {

    private _id: string|number;
    get id(): string|number { return this._id }
    set id(_id: string|number) { this._id = _id }

    private _uid: string;
    get uid(): string { return this._uid }
    set uid(_uid: string) { this._uid = _uid }

    private _companyName: string;
    get companyName(): string { return this._companyName }
    set companyName(_companyName: string) { this._companyName = _companyName }
    
    private _offices: Office[] = [];
    get offices(): Office[] { return this._offices; }
    set offices(_offices: Office[]) { this._offices = _offices }

    private _logo: string;
    get logo(): string { return this._logo }
    set logo(_logo: string) { this._logo = _logo }    

    private _officesCount: number;
    get officesCount(): number { return this._officesCount }
    set officesCount(_officesCount: number) { this._officesCount = _officesCount }

    constructor() {
        this._id = 0;
        this._uid = '';
        this._companyName = '';
        this._logo = '';
    }

    /**
     * 
     * @param _data 
     */
    public static toClientObject(_data: any): Company {
        let _company: Company = new Company();
        _company.logo = _data.logo? _data.logo : Asset.PROFILE_NO_PICTURE;
        _company.companyName = _data.tradeName;
        _company.id = _data.id;
        _company.uid = _data.uid;

        if (_data.companyAddressesQuantity) {
            _company.officesCount = _data.companyAddressesQuantity;
        }

        return _company;
    }
    
}