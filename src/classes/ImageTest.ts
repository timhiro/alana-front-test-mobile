export class ImageTest {
    
        private _id: number;
        get id(): number { return this._id; };
        set id(_id: number) { this._id = _id; };
    
        public _imageUrl: string;
        get imageUrl(): string { return this._imageUrl; };
        set imageUrl(_imageUrl: string) { this._imageUrl = _imageUrl; };
    
        public isSelected: boolean;
        get _isSelected(): boolean { return this.isSelected; };
        set _isSelected(isSelected: boolean) { this.isSelected = isSelected; };

        constructor(_id?: number, _url?: string) {
            this._id = _id;
            this.imageUrl = _url;
            this._isSelected = false;
        }

    }