export class User{

    private _email: string = '';
    get email(): string { return this._email; }
    set email(_email: string) { this._email = _email; }

    private _password: string = '';
    get password(): string { return this._password; }
    set password(_password: string) { this._password = _password; } 

    private _token: string = '';
    get token(): string { return this._token; }
    set token(_token: string) { this._token = _token; } 

    constructor() {
        this._email = '';
        this._password = '';
        this._token = '';
    }
}