export class JobPosition {

    public _id: number;
    get id(): number { return this._id; }
    set id(_id: number) { this._id = _id; }

    public _jobPositionName: string;
    get jobPositionName(): string { return this._jobPositionName; }
    set jobPositionName(_jobPositionName: string) { this._jobPositionName = _jobPositionName; }

    public _experience: number;
    get experience(): number { return this._experience; }
    set experience(_experience: number) { this._experience = _experience; }

    private _iconUrl: string;
    get iconUrl(): string { return this._iconUrl; }
    set iconUrl(_iconUrl: string) { this._iconUrl = _iconUrl; }

    private _companyExperience: string;
    get companyExperience(): string { return this._companyExperience; }
    set companyExperience(_companyExperience: string) { this._companyExperience = _companyExperience; }

    constructor (_id?: number, _name?: string, _icon?: string) {
        this._id = _id;
        this._jobPositionName = _name;
        this._iconUrl = _icon;
    }

    public static toServerRequest(_instance: JobPosition): any {
        let _data = {
            id: _instance.id,
            jobPositionName: _instance.jobPositionName,
            experience: _instance.experience,
            iconUrl: _instance.iconUrl,
            companyExperience: _instance.companyExperience
        }

        return _data;
    }

}