import { Address } from "./Address";
import { Company } from "./company-classes/Company";
import { JobPosition } from "./JobPosition";

export class Interview {

    /**
     * 
     */
    private _date: Date;
    get date(): Date { return this._date; }
    set date(_date: Date) { this._date = _date; }

    /**
     * 
     */
    private _address: Address;
    get address(): Address { return this._address; }
    set address(_address: Address) { this._address = _address; }

    /**
     * 
     */
    private _addressDescription: string;
    get addressDescription(): string { return this._addressDescription; }
    set addressDescription(_addressDescription: string) { this._addressDescription = _addressDescription; }

    /**
     * 
     */
    private _asistance: boolean;
    get asistance(): boolean { return this._asistance; }
    set asistance(_asistance: boolean) { this._asistance = _asistance; }

    /**
     * 
     */
    private _reminder: boolean;
    get reminder(): boolean { return this._reminder; }
    set reminder(_reminder: boolean) { this._reminder = _reminder; }

    /**
     * Workaround
     */
    private _publicationUid: string;
    get publicationUid(): string { return this._publicationUid; }
    set publicationUid(_uid: string) { this._publicationUid = _uid; }

    /**
     * Workaround
     */
    private _candidateUid: string;
    get candidateUid(): string { return this._candidateUid; }
    set candidateUid(_uid: string) { this._candidateUid = _uid; }

    /**
     * Workaround
     */
    private _candidate: any;
    get candidate(): any { return this._candidate; }
    set candidate(_candidate: any) { this._candidate = _candidate; }

    /**
     * Workaround
     */
    private _company: Company;
    get company(): Company { return this._company; }
    set company(_company: Company) { this._company = _company; }

    /**
     * Workaround
     */
    private _job: JobPosition;
    get job(): JobPosition { return this._job; }
    set job(_job: JobPosition) { this._job = _job; }

    constructor() {
        this._date = new Date();
        this.address = new Address();
    }

    public static toServerRequest(_instance: Interview): any {
        // DD/MM/YYYY HH:MM:SS
        let _dateFormated: string = `${_instance.date.getDate() < 10? `0${_instance.date.getDate()}` : _instance.date.getDate() }/${(_instance.date.getMonth() + 1) < 10? `0${_instance.date.getMonth() + 1}` : _instance.date.getMonth() + 1}/${_instance.date.getFullYear()}`;
        let _timeFormated: string = `${_instance.date.getHours() < 10 ? `0${_instance.date.getHours()}` : _instance.date.getHours() }:${_instance.date.getMinutes() < 10? '00' : _instance.date.getMinutes()}:00`;

        let _data = {
            candidate: _instance.candidateUid,
            publication: _instance.publicationUid,
            addressName: _instance._address.name,
            addressLongitude: _instance.address.longitude,
            addressLatitude: _instance.address.latitude,
            location: _instance.address.countryId,
            description: _instance._addressDescription,
            date: `${_dateFormated} ${_timeFormated}`,
            statusNotification: _instance.reminder? 98 : 99  // 98 turn on // 99 turn off
        }

        return _data;
    }

    public static toClientObject(_data: any, _role: string): Interview {
        let _instance: Interview = new Interview();

        const _interviewDate = _data.interview.date.split('/');
        const _interviewTime = _data.interview.time.split(':');
        _instance.setInterviewDate(parseInt(_interviewDate[0]), parseInt(_interviewDate[1]), parseInt(_interviewDate[2]));
        _instance.setInterviewTime(parseInt(_interviewTime[0]), parseInt(_interviewTime[1]));

        if (_role == 'candidate') {
            let _address = new Address(); 
            _address.name = _data.interview.address.name;
            _address.latitude = _data.interview.address.latitude;
            _address.longitude = _data.interview.address.longitude;
            _instance.address = _address;
            _instance.addressDescription = _data.interview.address.description;
            _instance.company = new Company();
            _instance.company.logo = _data.interview.company.logo;
            _instance.company.companyName = _data.interview.company.name;
            _instance.job = new JobPosition(null, _data.interview.publication.publication.jobPosition.jobPositionName, _data.interview.publication.publication.jobPosition.icon);
        } else if (_role == 'company') {
            //FIXME: Usar candidate o match
            _instance.candidate = _data.interview.candidate;
        }
        return _instance;
    }

    /**
     * 
     */
    public interviewMinYear() {
        return new Date().toISOString();
    }

    /**
     * 
     */
    public interviewMaxYear(): string {
        return (new Date().getUTCFullYear() + 1).toString();
    }

    /**
     * 
     * @param _day 
     * @param _month 
     * @param _year 
     */
    public setInterviewDate(_day: number, _month: number, _year: number): void {
        this._date.setDate(_day);
        this._date.setMonth(_month - 1);
        this._date.setFullYear(_year);
    }

    /**
     * 
     * @param _hour 
     * @param _minutes 
     */
    public setInterviewTime(_hour: number, _minutes: number): void {
        this._date.setHours(_hour);
        this._date.setMinutes(_minutes);
    }

    /**
     * 
     */
    public getDate(): string {
        let _dateText: string;
        const _days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        const _months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        _dateText = `${_days[this._date.getDay()]} ${this._date.getDate()} de ${_months[this._date.getMonth()]}, ${this._date.getFullYear()}`;

        return _dateText;
    }

    /**
     * 
     */
    public getTime(): string {
        let _time: string;
        _time = `${this._date.getHours() < 10? `0${this._date.getHours().toString()}` : this._date.getHours().toString()}:${this._date.getMinutes() < 10? `0${this._date.getMinutes()}` : this._date.getMinutes()}`;

        return _time;
    }
}