import { CountryCode } from "./CountryCode";

export class Phone {

    private _phoneNumber: string;
    get phoneNumber(): string { return this._phoneNumber; }
    set phoneNumber(_phoneNumber: string) { this._phoneNumber = _phoneNumber; }

    private _countryCode: CountryCode;
    get countryCode(): CountryCode { return this._countryCode; }
    set countryCode(_countryCode: CountryCode) { this._countryCode = _countryCode; }

    constructor() {
        this._phoneNumber = '';
        this._countryCode = new CountryCode();
    }
}