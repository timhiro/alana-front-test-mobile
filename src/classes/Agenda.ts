import { Interview } from "./Interview";

export class Agenda {
    
    /**
     * 
     */
    private _date: Date;
    get date(): Date { return this._date; }
    set date(_date: Date) { this._date = _date; }

    /**
     * 
     */
    private _interviews: Interview[];
    get interviews(): Interview[] { return this._interviews; }
    set interviews(_interviews: Interview[]) { this._interviews = _interviews; }

    constructor() {
        this._date = new Date();
        this._interviews = [];
    }
    
    public static toClientObject(_data: any): Agenda {
        let _agenda: Agenda = new Agenda();
        let _date = _data.date.split('/');

        _agenda.date.setDate(parseInt(_date[0]));
        _agenda.date.setMonth(parseInt(_date[1]) - 1);
        _agenda.date.setFullYear(parseInt(_date[2]));

        for (let _dataInterview of _data.interviews) {
            let _interview = Interview.toClientObject(_dataInterview, 'company');
            _agenda._interviews.push(_interview);
        }

        return _agenda;
    }

    /**
     * 
     */
    public getAgendaDate(): string {
        let _text: string;
        const _days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        const _months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        _text = `${_days[this.date.getDay()]} ${this.date.getDate()} de ${_months[this.date.getMonth()]}, ${this.date.getFullYear()}`;

        return _text;
    }

    /**
     * 
     */
    public getRemainingCandidates(): string {
        let _remaining: number = this.interviews.length - 4;
        return _remaining > 0? _remaining.toString() : ''; 
    }
}