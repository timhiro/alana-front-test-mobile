export class Schedule {

    public anySchedule: boolean = false;

    public minHourSelected: string = '';

    public maxHourSelected: string = '';

    public minAvailability: number = 0;

    public maxAvailability: number = 0;

    public minMeridem: string = '';

    public maxMeridem: string = '';

    constructor (_min?: number, _max?: number) {
        if (_min >= 0 && _max >= 0) {
            this.minAvailability = _min;
            this.maxAvailability = _max;    
        }
    }

    /**
     * Get schedule information text.
     * (DEPRECATED)
     */
    public getScheduleInformation(): string {
        let _info: string;

        if(this.minAvailability == 0 && this.maxAvailability >= 1439) {
            _info = 'Cualquier horario';
        } else {
            let _minSchedule: string = '', _maxSchedule: string = '';
            _minSchedule = (Math.round(this.minAvailability) / 60).toString() + ':00';
            _maxSchedule = (Math.round(this.maxAvailability) / 60).toString() + ':00';

            _info = _minSchedule + ' - ' + _maxSchedule;
        }
        
        return _info;
    }

    /**
     * 
     */
    public isAnySchedule(): boolean {
        return (this.minAvailability == 0 && this.maxAvailability >= 1439);
    }

    /**
     * 
     * @param _isMin 
     */
    public getScheduleHour(_isMin: boolean): string {
        let _scheduleHour: string = '';
        const _hour: number = _isMin? (Math.round(this.minAvailability) / 60) : (Math.round(this.maxAvailability) / 60);
        
        if (_hour < 12) {
            _scheduleHour = _hour.toString() + ':00';
            _isMin? this.minMeridem = 'am' : this.maxMeridem = 'am';
        } else if(_hour > 23) {
            _scheduleHour = (_hour - 12).toString() + ':00';
            _isMin? this.minMeridem = 'am' : this.maxMeridem = 'am';
        } else {
            _scheduleHour = (_hour - 12).toString() + ':00';
            _isMin? this.minMeridem = 'pm' : this.maxMeridem = 'pm';
        }

        return _scheduleHour;
    }

    /**
     * Set the availability number to hours.
     * Min 0 and max 1439 means any scheudule.
     */
    public setAvailabilityToHours(): void{
        if (this.minAvailability == 0 && this.maxAvailability == 1439) {
            this.anySchedule = true;
        }
        else {
            this.minHourSelected = `${this.minAvailability <= 540? '0' : ''}${this.minAvailability / 60}:00`;
            this.maxHourSelected = (this.maxAvailability / 60).toString() + ':00';
        }
    }

    /**
     * Set the min and max hour values.
     */
    public setScheldule(): boolean {
        if (!this.anySchedule && (this.maxHourSelected != '' && this.minHourSelected != '')) {
            let _maxValue = this.maxHourSelected.split(':');
            let _minValue = this.minHourSelected.split(':');

            this.minAvailability = Number.parseInt(_minValue[0]) * 60;
            this.maxAvailability = Number.parseInt(_maxValue[0]) * 60;

            return true;
            
        } else if (this.anySchedule) {
            this.minAvailability = 0;
            this.maxAvailability = 24 * 60 - 1; // server validation 23:59
            
            return true;
        }

        return false
    }     

}