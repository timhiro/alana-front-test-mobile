export class Address {

    public name: string = '';

    public countryId: number = null;

    public country: string = '';

    public latitude: number = 0;

    public longitude: number = 0;

    constructor (_name?: string, _latitude?: number, _longitude?: number, _country?: string) {
        this.name = _name;
        this.latitude = _latitude;
        this.longitude = _longitude;
        this.country = _country;
    }

    public static toClientObject(_data: any): Address {
        let _instance: Address = new Address(_data.name, _data.latitude, _data.longitude);
        _instance.countryId = _data.location.id;
        _instance.country = _data.location.name;
        
        return _instance;
    }
}