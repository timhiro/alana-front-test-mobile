import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'alana-header',
    templateUrl: 'alana-header.component.html'
})
export class AlanaHeaderComponent {
    /**
     * 
     */
    @Input('dismissAnimation') animate: boolean;
    /**
     * 
     */
    @Input('displayBackButton') displayBackButton: boolean;
    /**
     * 
     */
    @Input('customBackAction') customBackAction: boolean;
    /**
     * 
     */
    @Input('segment') segment: string[];
    /**
     * 
     */
    @Input('segmentValue') segmentValue: number;

    /**
     * 
     */
    @Output('backAction') backActionEmitter: EventEmitter<boolean>;
    /**
     * 
     */
    @Output('onSegmentChange') onSegmentChangeEmitter: EventEmitter<number>;

    constructor(
        public navCtrl: NavController
    ) {
        this.animate = false;
        this.displayBackButton = true;
        this.customBackAction = false;
        this.segmentValue = 0;

        this.backActionEmitter = new EventEmitter<boolean>();
        this.onSegmentChangeEmitter = new EventEmitter<number>();
    }

    /**
     * 
     * @param _value 
     */
    public selectSegment(_value: number): void {
        this.segmentValue = _value;
        this.onSegmentChangeEmitter.emit(_value);
    }

    /**
     * 
     */
    public returnPrevPage(): void {
        if (!this.customBackAction) {
            const _options = {animate: this.animate? true : false, animation: 'md-transition'}
            this.navCtrl.pop(_options);
        } else {
            this.backActionEmitter.emit(true);
        }
    }

}
