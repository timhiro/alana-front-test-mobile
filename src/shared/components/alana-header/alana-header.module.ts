import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

import { AlanaHeaderComponent } from './alana-header.component';

@NgModule({
    declarations: [
        AlanaHeaderComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
    exports: [
        AlanaHeaderComponent
    ]
})

export class AlanaHeaderModule { }
