import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UtilService } from '../../../services/util.service';

@Component({
    selector: 'alana-item',
    templateUrl: 'alana-item.component.html'
})
export class AlanaItemComponent {

    /**
     * Item background color.
     */
    @Input('background') public backgroundColor: string = '#fffff';
    /**
     * Item thumbnail image.
     */
    @Input('thumbnail') public itemThumbnail: string = '';
    /**
     * Item layout size.
     */
    @Input('size') public itemSize: string;
    /**
     * Item action icon.
     */
    @Input('actionIcon') public actionIcon: string = '';
    /**
     * 
     */
    @Input('selectable') public isSelectable: boolean = false;
    /**
     * Show a default image for server images (Default is true).
     */
    @Input('defaultImage') public defaultImage: boolean = true;
    /**
     * Item tap action.
     */
    @Output('action') public actionEmitter: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Define the item layout size using "vh" units.
     */
    private itemClass: string = '';

    constructor(
        public util: UtilService
    ) {}

    ngOnInit() {
        switch(this.itemSize) {
            case 'large':
                this.itemClass = 'item-large';
                break;
            case 'medium':
                this.itemClass = 'item-medium';
                break;
            case 'small':
                this.itemClass = 'item-small';
                break;
            default:
                this.itemClass = 'item-large';
        }
    }

}
