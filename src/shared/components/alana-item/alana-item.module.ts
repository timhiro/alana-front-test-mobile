import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

import { AlanaItemComponent } from './alana-item.component';

@NgModule({
  declarations: [
    AlanaItemComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [
    AlanaItemComponent
  ]
})

export class AlanaItemModule { }
