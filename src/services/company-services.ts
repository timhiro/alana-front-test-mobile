import { RequestOptions, Http, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/delay';
import { Path } from '../util/Path';
import { KeyValue } from '../util/KeyValue';
import { Company } from '../classes/company-classes/Company';
import { Office } from '../classes/company-classes/Office';


@Injectable()
export class  CompanyService{


    /**
     * Current company user role.
     */
    private _currentCompanyId: string = '';
    set companyId(_id: string) { this._currentCompanyId = _id; }

    private _currentOfficeId: string = '';
    set officeId(_id: string) { this._currentOfficeId = _id; }

    constructor(
        private http: Http,
    ) { 
    }

    /**
     * Método para obtener los datos de un Manager o usuario de empresa.
     */
    public getCompanyUserInformation(): Observable<any> {
        return this.http.get(Path.getDomain(Path.COMPANY_USER_INFO), this.jwtCompany())
        .map((response: Response) => {
            const _data = response.json();
            return <any> _data;
        })
    }

    /**
     * Método para obtener una lista de sedes (Office) correspondiente a una empresa (Company).
     * FIXME: Al parecer posee un error y no se obtienen los datos del servidor
     */
    public getCompanyOffices(_company: Company): Observable<Office[]> {
        return this.http.get(Path.getDomain(Path.COMPANY_COMPANY_ADDRESS_LIST), this.jwtCompany())
        .map((response: Response) => {
            const _data = response.json().response;
            let _offices: Office[] = [];
            for (let _officeEntity of _data) {
                _offices.push(Office.toClientObject(_officeEntity));
            }

            return _offices;
        })
    }

    /**
     * Método para asignar los Headers de la petición.
     * Una petición segura requiere de un token en el header.
     * Existen parámetros opcionales para asignar ids correspondientes a una empresa o una sede
     */
    private jwtCompany(_addCompanyHeader?: boolean, _addOfficeHeader?: boolean): RequestOptions {
        let _user = JSON.parse(localStorage.getItem(KeyValue.USER));
        let _headers = new Headers({ 'Authorization': 'Bearer ' + _user._token});

        if (_addCompanyHeader) {
            _headers.append('company', this._currentCompanyId );
        }
        if (_addOfficeHeader) {
            _headers.append('company-address', this._currentOfficeId );
        }

        return new RequestOptions({ headers: _headers });

    }
}
