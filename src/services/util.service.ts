import { Asset } from "../util/Asset";
import { Navigation } from "../util/Navigation";
import { LoadingController, Loading } from "ionic-angular";
import { Injectable } from "@angular/core";

/**
 * 
 */
@Injectable()
export class UtilService {

    public Asset: typeof Asset = Asset;

    public Navigation: typeof Navigation = Navigation;

    private currentLoader: Loading = null;

    constructor(
        private loadingCtrl: LoadingController,
    ) {}


    /**
     * 
     */
    public displayLoader(): void {
        if(this.currentLoader == null){
            this.currentLoader = this.loadingCtrl.create({
                spinner: 'crescent',
                cssClass: 'loader-transparent'
            });
            this.currentLoader.present();
        }
    }

    /**
     * 
     */
    public stopLoader(): void {
        if(this.currentLoader != null){
            this.currentLoader.dismiss();
            this.currentLoader = null;
        }
    }
}