import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { User } from '../classes/User';
import { Path } from '../util/Path';
import { KeyValue } from '../util/KeyValue';


/**
 * 
 */
@Injectable()
export class AuthenticationService {

    private currentUser: User;

    constructor(
        private http: Http,
    ) { 
        this.currentUser = new User();
    }

        /**
     * 
     * @param _newUser 
     */
    public companyLogIn(_newUser: User): Observable<any> {
        let body = 'email=' + _newUser.email + '&password=' + _newUser.password;
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(Path.getDomain(Path.COMPANY_LOG_IN), body, { headers: headers })
            .map((response: Response) => {
                let apiData = response.json();
                if (apiData && apiData.token) {
                    this.currentUser = _newUser;
                    this.currentUser.token = apiData.token;
                    localStorage.setItem(KeyValue.USER, JSON.stringify(this.currentUser));
                }
            })
            .catch((error: any) => Observable.throw(error.json()))
    }
}