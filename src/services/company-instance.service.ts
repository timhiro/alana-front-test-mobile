import { Injectable } from '@angular/core';

import { Manager } from '../classes/company-classes/Manager';
import { Company } from '../classes/company-classes/Company';
import { CompanyService } from './company-services';

@Injectable()
export class CompanyInstanceService {

    /**
     * 
     */
    private _manager: Manager;
    get manager(): Manager { return this._manager; }

    /**
     * 
     */
    private _company: Company;
    get company(): Company { return this._company; }


    constructor(
        private companyService: CompanyService,
    ) {

    }

    /**
     * 
     */
    public getManagerInformation(): Promise<boolean> {
        return new Promise<boolean>(
            resolve => {
                this.companyService.getCompanyUserInformation()
                .subscribe(
                    data  => {
                        let _entity = data.response;
                        this._manager = Manager.toClientObject(_entity, true);
                        if (this._manager.enterprise.companies.length) {
                            this._company = this._manager.enterprise.getCompany(0)
                            this.companyService.companyId = this._company.id.toString();
                        }
                        resolve(true);
                    }, error => {
                        console.log(error);
                        resolve(false);
                    }
                );
            }
        );
    }

    /**
     * 
     * @param _company 
     */
    public setCompany(_company: Company): void {
        if (_company) {
            this._company = _company;
        }
    }

    /**
     * 
     * @param _manager 
     */
    public setManager(_manager: Manager): void {
        if (_manager) { 
            this._manager = _manager;
        }
    }
}
