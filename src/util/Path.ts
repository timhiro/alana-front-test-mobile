import { Domain } from './Domain'
/**
 * Util class for API paths.
 */
export class Path {

    /* Company paths */
    public static COMPANY_LOG_IN: string = 'secure-company/login_check';
    public static COMPANY_LOG_OUT: string = 'secure-company/logout';
    public static COMPANY_USER_INFO: string = 'secure-company/user/show';
    public static COMPANY_COMPANY_ADDRESS_LIST: string = 'secure-company/company-address/index';

    /**
     * Get the API domain.
     */
    public static getDomain(_path: string): string{
        return Domain.PROJECT_DOMAIN.domainName.concat(_path);
    }

    /**
     * Get the API domain with a required token.
     */
    public static getDomainWithToken(_path: string, _token: string): string{
        let _newPath = `${_path}/${_token}`;
        return Domain.PROJECT_DOMAIN.domainName.concat(_newPath);
    }

    /**
     * Get the API domain with a some query string params.
     * @param _path
     * @param _params
     */
    public static getDomainWithParams(_path: string, _params: string){
        let _pathWithParams = `${_path}?${_params}`;
        return Domain.PROJECT_DOMAIN.domainName.concat(_pathWithParams);
    }
}
