export class StatusCode{

    public static STATUS_STATUS_ENABLE: number = 1;
    public static STATUS_STATUS_DISABLED: number = 2;
    public static STATUS_STATUS_CLOSED: number = 3;
    public static STATUS_STATUS_AVAILABLE: number = 4;
    public static STATUS_STATUS_UNAVAILABLE: number = 5;
    public static STATUS_STATUS_CHECKED: number = 15;
    public static STATUS_STATUS_UNCHECKED: number = 16;
    public static STATUS_STATUS_SENT: number = 17;
    public static STATUS_STATUS_WAITING: number = 18;
    public static STATUS_STATUS_APPROVED: number = 29;
    public static STATUS_STATUS_DISAPPROVED: number = 30;

    /*Civil status id's*/
    public static STATUS_CIVIL_STATUS_SINGLE: number = 6;
    public static STATUS_CIVIL_STATUS_MARRIED: number = 7;
    public static STATUS_CIVIL_STATUS_WIDOWER: number = 8;
    public static STATUS_CIVIL_STATUS_DIVORCED: number = 9;

    /*Location id's*/
    public static STATUS_LOCATION_COUNTRY: number = 12;
    public static STATUS_LOCATION_STATE: number = 13;
    public static STATUS_LOCATION_CITY: number = 14;

    /*Company type id's*/
    public static STATUS_COMPANY_TYPE_LEGAL: number = 19;
    public static STATUS_COMPANY_TYPE_NATURAL: number = 20;

    /*Notification type id's*/
    public static STATUS_NOTIFICATION_TYPE_CANDIDATE_MATCHES_PUBLICATION: number = 21;
    public static STATUS_NOTIFICATION_TYPE_CANDIDATE_SUCCESS_ANSWER: number = 22;
    public static STATUS_NOTIFICATION_TYPE_COMPANY_INTERESTED_CANDIDATE: number = 23;
    public static STATUS_NOTIFICATION_TYPE_COMPANY_10_DAYS_REMAINING_TRIAL: number = 24;
    public static STATUS_NOTIFICATION_TYPE_COMPANY_5_DAYS_REMAINING_TRIAL: number = 25;
    public static STATUS_NOTIFICATION_TYPE_COMPANY_1_DAY_REMAINING_TRIAL: number = 26;
    public static STATUS_NOTIFICATION_TYPE_CANDIDATE_REJECTED: number = 27;
    public static STATUS_NOTIFICATION_TYPE_CANDIDATE_NEW_DATE: number = 28;

    /*Role type id's*/
    public static STATUS_ROLE_MANAGER: number = 31;
    public static STATUS_ROLE_ASSISTANT: number = 32;
    public static STATUS_ROLE_SUPER_MANAGER: number = 113;
    public static STATUS_ROLE_ADMIN: number = 33;
    public static STATUS_ROLE_CANDIDATE: number = 34;

    /* Gender */
    public static MALE: number = 35;
    public static FEMALE: number = 36;
    public static GENDER_OTHER: number = 37;

    /* Social Network */
    public static FACEBOOK_LOG_IN: number = 38;
    public static GOOGLE_LOG_IN: number = 39;

    /* experience */
    public static EXPERIENCE_LOW: number = 0;
    public static EXPERIENCE_MID: number = 20;
    public static EXPERIENCE_HIGH: number = 60;

    /* Call feedback */
    public static CALL_INTERVIEW_INVITE: number = 56;
    public static CALL_DISCARD: number = 57;
    public static CALL_NO_ANSWER: number = 58;

    /* Discard question */
    public static STATUS_TYPE_DISCARD_FROM_CONNECTION = 63;
    public static STATUS_TYPE_DISCARD_FROM_LIKE = 64;
    public static STATUS_TYPE_DISCARD_AFTER_CONTACT = 65;
    
    /* CHAT USER */
    public static COMPANY_CHAT_USER: number = 120;
    public static CANDIDATE_CHAT_USER: number = 121;

    /* FEEDBACK */
    public static FEEDBACK_CHAT: number = 75;

    public static CANDIDATE_USER: number = 100;
    public static COMPANY_USER: number = 101;

    /* CHAT BOT STATUS DAY */
    public static STATUS_DAY_MONDAY = 76;
    public static STATUS_DAY_TUESDAY = 77;
    public static STATUS_DAY_WEDNESDAY = 78;
    public static STATUS_DAY_THURSDAY = 79;
    public static STATUS_DAY_FRIDAY = 80;
    public static CHAT_BOT_CLOSED: number = 97;

    /* INTERVIEW */
    public static CHAT_INTERVIEW_POSITIVE_ANSWER: number = 88;
    public static CHAT_INTERVIEW_NEGATIVE_ANSWER: number = 89;

    /* REQUIREMENTS */
    public static STATUS_REQUIREMENT_PUBLICATION: number = 106;
    public static STATUS_REQUIREMENT_INTERVIEW: number = 107;

    /* PAYMENT HISTORY */
    public static STATUS_PAY_TYPE_OXXO = 119;
    public static STATUS_PAY_TYPE_SPEI = 120;
    public static STATUS_PAY_TYPE_CARD = 121;
    public static STATUS_PAY_TYPE_CORPORATE = 122;
}
