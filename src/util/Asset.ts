export class Asset{

    public static IMAGE_PROFILE_NO_PICTURE: string = './assets/imgs/image-picture-default.png';
    public static IMAGE_ALANA_LOGO: string = './assets/imgs/image-alana-logo.png';

    public static PROFILE_NO_PICTURE: string = './assets/imgs/image-picture-default.png';

    public static IMAGE_COMPANY_ELEMENTS_OFFICE: string = './assets/imgs/image-office.png';

    public static IMAGE_CARD: string = './assets/images/company-payments/image-card.png';
    public static IMAGE_CARD_SUCCESS: string = './assets/images/company-payments/image-card-success.png';
    public static IMAGE_CARD_ERROR: string = './assets/images/company-payments/image-card-error.png';
    public static IMAGE_MASTERCARD_LOGO: string = './assets/images/company-payments/image-mastercard-logo.png';
    public static IMAGE_VISA_LOGO: string = './assets/images/company-payments/image-visa-logo.png';
    public static IMAGE_GOOGLE_MAP: string = './assets/images/image-google-map.png';
    public static IMAGE_VISA_MASTERCARD_LOGO: string = './assets/images/company-payments/image-visa-mastercard.png';
    public static IMAGE_OXXO_PAY_LOGO: string = './assets/images/company-payments/image-oxxo-pay.png';
    public static IMAGE_OXXO_PAY_MINI_LOGO: string = './assets/images/company-payments/image-oxxo-pay-mini.png';
    public static IMAGE_SPE_LOGO: string = './assets/images/company-payments/image-spei.png';
    public static IMAGE_SPE_MINI_LOGO: string = './assets/images/company-payments/image-spei-mini.png';
    public static IMAGE_CORPORATE: string = './assets/images/company-payments/image-corporate.png';

    /* Company Elements */
    public static IMAGE_COMPANY_ELEMENTS_PUBLICATION: string = './assets/images/company-elements/publication.png';
    public static IMAGE_COMPANY_ELEMENTS_USERS: string = './assets/images/company-elements/users.png';
    public static IMAGE_COMPANY_ELEMENTS_STAFF: string = './assets/images/company-elements/staff.png';
    public static IMAGE_COMPANY_ELEMENTS_PURCHASES: string = './assets/images/company-elements/purchases.png';
    /* end Company Elements */

    /* Candidate public profile */
    public static IMAGE_CANDIDATE_MOTTO: string = './assets/images/candidate-public-profile/motto.png';
    public static IMAGE_CANDIDATE_SELFIE: string = './assets/images/candidate-public-profile/selfie.png';
    public static IMAGE_CANDIDATE_EXPERIENCE: string = './assets/images/candidate-public-profile/experience.png';
    public static IMAGE_CANDIDATE_LASTJOBS: string = './assets/images/candidate-public-profile/lastjob.png';
    public static IMAGE_CANDIDATE_SCHEDULE: string = './assets/images/candidate-public-profile/schedule.png';  
    public static IMAGE_CANDIDATE_PERSONAL_VALUES: string = './assets/images/candidate-public-profile/personal-values.png';    
    /* End candidato public profile */

    /* Deprecated */
    public static LOGO_WHITE: string = './assets/public/logo/alana.jpg';
    public static LOGO_RED: string = './assets/public/logo/alana-title.png';
    public static LOGO_ICON: string = './assets/public/logo/logo-icon.png'
    public static LOGO_PLAYSTORE: string = './assets/public/logo/play-store-logo.png'

    public static ICON_BRIEFCASE_RED: string = './assets/public/icons/briefcase-red.png';
    public static ICON_BRIEFCASE_GREY: string = './assets/public/icons/briefcase.png';
    public static ICON_BADGE_NOTIFICATION: string = './assets/public/icons/badge-notification.png';
    public static ICON_INVALID_INPUT: string = './assets/public/icons/invalid-input.png';
    public static ICON_EDIT_PENCIL: string = './assets/public/icons/edit-pencil.png';
    public static ICON_LOCATION_PIN: string = './assets/public/icons/marker.png';
    public static ICON_MANAGER_USER: string = './assets/public/icons/manager-user.png';
    public static ICON_MANAGER_ROL: string = './assets/public/icons/manager-rol.png';
    public static ICON_USER_ASSISTANT: string = './assets/public/icons/user-assistant-2.png';
    public static ICON_ADDRESS: string = './assets/public/icons/sedes.png';
    public static ICON_EMPTY: string = './assets/public/icons/empty.png';
    public static ICON_FILL: string = './assets/public/icons/fill.png';
    public static ICON_PHONE_CALL: string = './assets/public/icons/phone-call.png';
    public static ICON_HANDSHAKE: string = './assets/public/icons/handshake.png';

    public static GENERAL_ERROR: string = './assets/public/general/message-error.png';
    public static GENERAL_SUCCESS: string = './assets/public/general/message-success.png';
    public static GENERAL_VOUCHER_RED: string = 'assets/public/general/voucher-red.png';
    public static GENERAL_EMAIL_CHECK: string = './assets/public/general/checked.png';
    public static GENERAL_CANDIDATE: string = './assets/public/general/candidate.png';
    public static GENERAL_COMPANY: string = './assets/public/general/company.png';
    public static GENERAL_CHECK_MARK: string = './assets/public/general/check-mark.png';
    public static GENERAL_VOUCHER: string = './assets/public/general/voucher.png';
    public static GENERAL_FOLDER_JOB: string = './assets/public/general/folder-job.png';
    public static GENERAL_TEST_JOB: string = './assets/public/general/test-job.png';
    public static GENERAL_VIDEO_JOB: string = './assets/public/general/video-job.png';
    public static USER_LOGOUT: string = './assets/public/general/user-logout.png';
    public static USER_PASSWORD: string = './assets/public/general/user-password.png';
    public static CANDIDATE_REFERENCE_CODE: string = './assets/public/general/candidate-reference-code.png'
    public static GENERAL_USER_ADDRESS: string = './assets/public/general/user-address.png';
    public static GENERAL_USER_ADDRESSES: string = './assets/public/general/user-addresses.png';
    public static GENERAL_USER_EMAIL: string = './assets/public/general/user-email.png';
    public static GENERAL_VIDEO_SELFIE: string = './assets/public/general/mobile-phone.png';
    public static GENERAL_FILM_BACKGROUND: string = './assets/public/general/film-background.png';
    public static GENERAL_VIDEO_DURATION: string = './assets/public/general/duration-clock.png'
    public static GENERAL_USER_BRIEFCASE: string = './assets/public/general/briefcase.png';
    public static GENERAL_PLAY_BUTTON: string = './assets/public/general/play-button.png';
    public static GENERAL_USER_SMARTPHONE: string = './assets/public/general/smartphone.png';
    public static GENERAL_CANDIDATE_STATUS: string = './assets/public/general/candidate-status.png';
    public static GENERAL_CANDIDATE_MOBILE: string = './assets/public/general/mobile.png';
    public static GENERAL_VIDEO_THUMBNAIL: string = './assets/public/general/video-thumbnail.png';
    public static GENERAL_PHONE_ERROR: string = './assets/public/general/phoneError.png';
    public static GENERAL_DEFAULT_PUBLICATIONS: string = './assets/public/general/default-publications.png';
    public static GENERAL_USER_SCHEDULE: string = './assets/public/general/user-schedule.png';
    public static GENERAL_USER_INFO: string = './assets/public/general/user-info.png';
    public static GENERAL_USER_SECURITY: string = './assets/public/general/user-security.png';
    public static GENERAL_MANAGER_INFO: string = './assets/public/general/manager-info.png';
    public static GENERAL_COMPANY_NATURAL: string = './assets/public/general/company-natural.png';
    public static GENERAL_COMPANY_LEGAL: string = './assets/public/general/company-legal.png';
    public static GENERAL_COMPANY_ID: string = './assets/public/general/company-id.png';
    public static GENERAL_GOOGLE_MAPS_MARKER: string = './assets/public/general/google-maps-marker.png';

    public static GENERAL_MANAGER_TAXPAYER: string = './assets/public/general/manager-taxpayer.png'
    public static GENERAL_MANAGER_SETTINGS_COMPANY: string = './assets/public/general/manager-settings-company.png';
    public static GENERAL_MANAGER_SETTINGS_DATA: string = './assets/public/general/manager-settings-data.png';
    public static GENERAL_LIKE_HEART: string = 'assets/public/general/candidate-liked.png';
    public static GENERAL_VACANCY_APPLIED: string = './assets/public/general/vacancy-applied.png';
    public static GENERAL_VACANCY_INTERVIEW: string = './assets/public/general/vacancy-interview.png';
    public static GENERAL_LIKE_BUTTON: string = './assets/public/general/like-button.png';
    public static GENERAL_DISLIKE_BUTTON: string = './assets/public/general/dislike-button.png';
    public static GENERAL_ID_CARD: string = './assets/public/general/id-card.png';
    public static GENERAL_PHONE_RECOGNITION: string = './assets/public/general/phone-recognition.png';
    public static GENERAL_EMPTY_COMPANY: string = './assets/public/general/empty-company.png';
    public static GENERAL_MONEY: string = './assets/public/general/money.png';
    public static GENERAL_USER_SEARCH: string = './assets/public/general/user-search.png';
    public static GENERAL_USER_EXPERIENCE: string = './assets/public/general/experience.png';
    public static GENERAL_USER_REQUIREMENTS: string = './assets/public/general/requirements.png';
    public static GENERAL_USER_BENEFITS: string = './assets/public/general/benefits.png';
    public static GENERAL_VIDEO_FLICK_LEFT: string = 'assets/public/general/flick-to-left.png';
    public static GENERAL_VIDEO_FLICK_SIDES: string = 'assets/public/general/flick-sides.png';
    public static GENERAL_ROBOT: string = './assets/public/general/robot.png';
    public static GENERAL_REFERRAL_INVITATIONS: string = './assets/public/general/referral-invitations.png';
    public static GENERAL_REFERRAL_PROFILES: string = './assets/public/general/referral-profiles.png';
    public static GENERAL_REFERRAL_HIRED: string = './assets/public/general/referral-hired.png';

}
