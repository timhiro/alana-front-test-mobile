/**
 * Key values class for key strings used in localstorage.
 */
export class KeyValue{

    public static USER: string = 'currentUser';

    public static INTRO: string = 'intro';
    public static DEVICE_ID: string = 'device_id';
    public static DEVICE_MODEL: string = 'device_model';
    public static IS_CANDIDATE_LOGIN_PAGE: string = 'candidateLogin';
    public static COMPANY_ADDRESS_SELECTION: string = 'companyAddressSelection';
    public static COMPANY_CANDIDATE_APPLY: string = 'companyCandidateApply';
    public static COMPANY_CANDIDATE_INTERVIEW: string = 'companyCandidateInterview';
    public static COMPANY_CANDIDATE_FIRST_LIKE: string = 'companyCandidateFirstLike';
    public static COMPANY_CANDIDATE_FIRST_DISLIKE: string = 'companyCandidateFirstDislike';
    public static STATUS_VALUE_POPUP: string = 'statusValuePopup';
    public static SYNC_NOTIFICATION: string = 'allowSyncNotification';
    public static AUTOMATIC_SYNC: string = 'allowAutomaticSync';
    public static PHONE_CODE_DATE: string = 'phoneCodeDate';

    public static RATE_APP: string = 'rateApp';

    public static CANDIDATE_ONBOARDING_PROFILE_COMPLETE: string = 'onboardingProfileComplete';
    public static CANDIDATE_ONBOARDING_PARTIAL_MATCH: string = 'onboardingPartialMatch';

    public static JOB_POSITION: string = 'jobPositions';
    public static COUNTRIES: string = 'countries';
    public static COUNTRY_CODES: string = 'countryCodes';
    public static FRANCHISES: string = 'franchises';
    public static TECHNICAL_KNOWLEDGE: string = 'technicalKnowledge';
    public static REQUIREMENTS: string = 'requirements';

    public static COMPANY_PUBLICATION_LIST: string = 'companyPublicationList';
    public static COMPANY_DETAILS : string = 'companyDetails';
    public static COMPANY_ADDRESS_LIST: string = 'companyAddress';
    public static COMPANY_USER_INFO: string = 'companyUserInfo';
    public static COMPANY_PUBLICATION_IMAGE_LIST: string = 'companyPublicationImageList';
    public static COMPANY_USER_LIST: string = 'companyUserList';

    public static CANDIDATE_INFO: string = 'candidateInfo';
    public static PROFILE_STEP: string = 'profileStep';
    public static PUBLICATION_INFO: string = 'publicationInfo';
    public static PROFILE_IMAGES_TEST: string = 'profileImagesTest';
    public static CANDIDATE_PERSONAL_VALUES: string = 'candidatePersonalValues';

    public static CURRENT_COMPANY: string = 'currentCompany'
    public static CURRENT_COMPANY_ADDRESS: string = 'currentCompanyAddress';
    public static COMPANY_CURRENT_USER_ROLE: string = 'currentUserRole';
}
