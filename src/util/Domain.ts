/**
 * Util class for domain configuration.
 */
export class Domain{

    public static PROJECT_DOMAIN: Domain = new Domain();

    /**
     * Domain name.
     */
    private _name: string;
    get domainName(): string { return this._name }
    /**
     * Domain web name.
     */
    private _webName: string;
    get domainWebName(): string { return this._webName }
    /**
     * Domain invitation reference value.
     */
    private _reference: string;
    get domainReference(): string { return this._reference }
    /**
     * 
     */
    private _conektaKey: string;
    get conektaKey(): string { return this._conektaKey }
    /**
     * Firebase configuration values.
     */
    private _firebase: any;
    get firebaseConfig(): string { return this._firebase }

    constructor() {
        this.setDevDomain();
        Domain.PROJECT_DOMAIN = this;

    }

    /**
     * 
     */
    public setDevDomain(): void {
        this._name = 'https://apidev.alanajobs.com/';
        this._webName = 'https://websitetesting.alana.jobs/';
        this._reference = 'https://webappdev.alana.jobs/invite/';
        this._firebase = {
            apiKey: "AIzaSyC1OqqcrOM0Wc0cbFg3lqS-nfckqEUo8po",
            authDomain: "project-5593880575436658706.firebaseapp.com",
            databaseURL: "https://alana-chat-dev.firebaseio.com/",
            projectId: "project-5593880575436658706",
            storageBucket: "project-5593880575436658706.appspot.com",
        }
        this._conektaKey = 'key_M9sNWbg72xztuCkDPguCELA';
    }
}