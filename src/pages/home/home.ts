import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { UtilService } from '../../services/util.service';
import { CompanyService } from '../../services/company-services';
import { CompanyInstanceService } from '../../services/company-instance.service';
import { Company } from '../../classes/company-classes/Company';
import { Office } from '../../classes/company-classes/Office';

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    /**
     * 
     */
    public showContent: boolean;
    /**
     * 
     */
    public currentCompany: Company;

    constructor(
        public navCtrl: NavController,
        public util: UtilService,  
        private companyService: CompanyService,
        private instanceService: CompanyInstanceService,
    ) {
        this.showContent = false;
        this.currentCompany = this.instanceService.company;
        this.getOffices();
    }


    /**
     * 
     */
    public getOffices(): void {
        if (this.currentCompany) {
            this.companyService.getCompanyOffices(this.currentCompany)
            .finally(() => {
                if (!this.showContent) {
                    this.showContent = true;
                }
            })
            .subscribe(
                data => {
                    this.currentCompany.offices = data;
                    this.instanceService.setCompany(this.currentCompany);
                }
            );
        }
    }

    public searchPublications(_office: Office):void {
        console.log('Buscar publicaciones de esta sede', _office);
    } 
}
