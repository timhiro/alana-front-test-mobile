import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { AlanaHeaderModule } from '../../shared/components/alana-header/alana-header.module';
import { AlanaItemModule } from '../../shared/components/alana-item/alana-item.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    AlanaHeaderModule,
    AlanaItemModule,
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}
