import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UtilService } from '../../services/util.service';
import { User } from '../../classes/User';
import { AuthenticationService } from '../../services/authentication-service';
import { CompanyInstanceService } from '../../services/company-instance.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    /**
     * 
     */
    public currentUser: User;
    /**
     * 
     */
    public isPasswordVisible: boolean;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public util: UtilService,
        private authticationService: AuthenticationService,
        private instanceService: CompanyInstanceService,
        ) {
            this.currentUser = new User();
            this.isPasswordVisible = false;
    }

    ionViewDidLoad() {
    }

/**
     * Custom login with an account stored
     */
    public companyLogin(): void {
        if (this.currentUser.email != '' && this.currentUser.password != '') {
            this.util.displayLoader();
            this.authticationService.companyLogIn(this.currentUser)
            .finally( () => {
                this.util.stopLoader();
            })
            .subscribe(
                () => {
                    this.instanceService.getManagerInformation()
                    .then(
                        (success) => {
                            if (success) {
                                this.navCtrl.setRoot(this.util.Navigation.PAGE_HOME);
                            }
                        }
                    );               
                }, err => {
                    console.log(err);
                });
        }
    }
}
